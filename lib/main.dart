// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:wallet/slider_widget.dart';
import 'package:web3dart/web3dart.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'PKCoin',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Client httpClient;
  late Web3Client ethClient;
  bool data = false;
  int myAmount = 0;
  final myAdress = "0xd072Dc5dC75DA2A6E148019D757C4B3595C2fb02";
  var myData;
  late String txHash;
  @override
  void initState() {
    super.initState();
    httpClient = Client();
    ethClient = Web3Client(
        "https://rinkeby.infura.io/v3/f16f491377684c4896fbb41660c12630",
        httpClient);

    getBalance(myAdress);
  }

  Future<DeployedContract> loadContract() async {
    String abi = await rootBundle.loadString("assets/abi.json");
    String contractAddress = "0x6563c4dE97bFC43bd6E09e251fBbFbDcDCfb872b";
    final contract = DeployedContract(ContractAbi.fromJson(abi, "PKCoin"),
        EthereumAddress.fromHex(contractAddress));
    return contract;
  }

  Future<List<dynamic>> query(String functionName, List<dynamic> args) async {
    final contract = await loadContract();
    final etherFunction = contract.function(functionName);
    final result = await ethClient.call(
        contract: contract, function: etherFunction, params: args);
    return result;
  }

  Future<void> getBalance(String targetAdress) async {
    EthereumAddress address = EthereumAddress.fromHex(targetAdress);
    List<dynamic> result = await query("getbalace", []);
    myData = result[0];
    data = true;
    setState(() {});
  }

  Future<String> submit(String functionName, List<dynamic> args) async {
    EthPrivateKey credentials = EthPrivateKey.fromHex(
        "c797b37edcb0c9e1ca038a790b183a82e1d541e178123566155b4629b8c750a0");
    DeployedContract contract = await loadContract();
    final etherFunction = contract.function(functionName);

    final result = await ethClient.sendTransaction(
        credentials,
        Transaction.callContract(
            contract: contract, function: etherFunction, parameters: args),
        //fetchChainIdFromNetworkId: true,
        chainId: 4
        //fetchChainIdFromNetworkId: true
        );
    return result;
  }

  Future<String> sendCoin() async {
    var bigAmount = BigInt.from(myAmount);
    var response = await submit("depositbalance", [bigAmount]);
    txHash = response;
    setState(() {});
    print("Deposited");
    return response;
  }

  Future<String> withdrawCoin() async {
    var bigAmount = BigInt.from(myAmount);
    var response = await submit("withdrotbalance", [bigAmount]);
    txHash = response;
    setState(() {});
    print("Withdrawn");
    return response;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Vx.gray300,
        body: ZStack([
          VxBox()
              .blue600
              .size(context.screenWidth, context.percentHeight * 30)
              .make(),
          VStack([
            (context.percentHeight * 10).heightBox,
            "\$PkCoin".text.xl4.white.bold.center.makeCentered().py16(),
            (context.percentHeight * 5).heightBox,
            VxBox(
                    child: VStack([
              "Balance".text.xl3.black.bold.center.makeCentered(),
              10.heightBox,
              data
                  ? "\$$myData".text.bold.xl6.makeCentered().shimmer()
                  : CircularProgressIndicator().centered()
            ]))
                .white
                .rounded
                .shadowXl
                .size(context.screenWidth, context.percentHeight * 18)
                .make(),
            30.heightBox,
            SliderWidget(
              min: 0,
              max: 10,
              finalVal: (value) {
                myAmount = (value * 10).round();
                // ignore: avoid_print
                print(myAmount);
              },
            ).centered(),
            HStack(
              [
                FlatButton.icon(
                        color: Colors.blue,
                        shape: Vx.roundedSm,
                        onPressed: () => getBalance(myAdress),
                        icon: const Icon(Icons.refresh, color: Colors.red),
                        label: "Refrech".text.white.make())
                    .h(50),
                FlatButton.icon(
                        color: Colors.green,
                        shape: Vx.roundedSm,
                        onPressed: () => sendCoin(),
                        icon: const Icon(Icons.call_made_outlined,
                            color: Colors.black),
                        label: "Deposit".text.white.make())
                    .h(50),
                FlatButton.icon(
                        color: Colors.red,
                        shape: Vx.roundedSm,
                        onPressed: () => withdrawCoin(),
                        icon: const Icon(Icons.call_received_outlined,
                            color: Colors.white),
                        label: "Withdraw".text.white.make())
                    .h(50)
              ],
              alignment: MainAxisAlignment.spaceAround,
              axisSize: MainAxisSize.max,
            ).p16(),
            if (txHash != null) txHash.text.black.makeCentered().p16()
          ])
        ]));
  }
}
